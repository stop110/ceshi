package Util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;


public class DbUtil {
	private static String dbUrl="jdbc:mysql://localhost:3306/zu";
	private static String dbName="root";
	private static String dbPs="root";
	private static String jdbcName="com.mysql.jdbc.Driver";
	public Connection getCon() throws Exception{
		Class.forName(jdbcName);
		Connection con = DriverManager.getConnection(dbUrl, dbName, dbPs);
		return con;
		}
	public void close(PreparedStatement pstmt,Connection con) throws Exception{
		if(pstmt!=null) {
			pstmt.close();
			if(con!=null) {
				con.close();
			}
		}
		
	}
}
