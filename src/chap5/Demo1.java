package chap5;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import Util.DbUtil;
import model.User;


public class Demo1 {
	private static DbUtil dbUtil = new DbUtil();
	private static void listUser()throws Exception{
		Connection con=dbUtil.getCon();
		String sql="select * from user";
		PreparedStatement pstmt=con.prepareStatement(sql);
		ResultSet rs=pstmt.executeQuery();
		while(rs.next()) {
			int id=rs.getInt(1);
			String username=rs.getString(2);
			String password=rs.getString(3);
			String type=rs.getString(4);
			System.out.println("账号ID:"+id+"管理员："+username+"管理员密码:"+password+"管理员分组："+type);
			System.out.println("==================================================");
		}
	}
	private static List<User> listUser1()throws Exception{
		List<User> userList=new ArrayList<User>();
		Connection con=dbUtil.getCon();
		String sql="select * from user";
		PreparedStatement pstmt=con.prepareStatement(sql);
		ResultSet rs=pstmt.executeQuery();
		while(rs.next()) {
			int id=rs.getInt("id");
			String username=rs.getString("username");
			String password=rs.getString("password");
			String type=rs.getString("type");
			User user=new User(id, username, password, type);
			userList.add(user);
		}
		dbUtil.close(pstmt, con);
		rs.close();
		return userList;
	}
	public static void main(String[] args) throws Exception {
		//listUser();
		List<User> userList=listUser1();
		for (User user : userList) {
			System.out.println(user);
		}
		
	}
}
