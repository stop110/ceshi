package chap6;

import java.sql.Connection;
import java.sql.PreparedStatement;

import Util.DbUtil;
import model.User;

public class Demo1 {
	private static DbUtil dbUtil=new DbUtil();
	private static int addUser(User user)throws Exception{
		Connection con=dbUtil.getCon();
		String sql="insert into user value(null,?,?,?)";
		PreparedStatement pstmt=con.prepareStatement(sql);
		pstmt.setString(1, user.getUsername());
		pstmt.setString(2, user.getPassword());
		pstmt.setString(3, user.getType());
		int result=pstmt.executeUpdate();
		dbUtil.close(pstmt, con);
		return result;
	}
	public static void main(String[] args) {
		
	}
}
